﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Firstrand.Controllers
{
    public class SiteLayoutController : SurfaceController
    {
            private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Layouts/";

            // Top Layout
            public ActionResult RenderTop()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_head.cshtml");
            }

            // Top Layout
            public ActionResult RenderBanner()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_banner.cshtml");
            }

            // SVG Logo
             public ActionResult RenderLogo()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_logo.svg");
            }
            // Site Menu (Top)
            public ActionResult RenderTopMenu()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_top_menu.cshtml");
            }

            // Header Layout
            public ActionResult RenderHeader()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_header.cshtml");
            }

            // Breadrumb Layout
            public ActionResult RenderBreadcrumb()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_breadcrumb.cshtml");
            }

            // Render the page banner
            public ActionResult RenderPageBanner()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_banner.cshtml");
            }

   
            // Footer Layout
            public ActionResult RenderFooter()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_footer.cshtml");
            }

            // Bottom Layout
            public ActionResult RenderBottom()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_bottom.cshtml");
            }

            // Bottom Layout
            public ActionResult RenderGalleryModal()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_GalleryModal.cshtml");
            }

            // Painting Classrooms Menu
            public ActionResult RenderPaintingClassroomsMenu()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_painting_classrooms_menu.cshtml");
            }
            // Painting Classrooms Menu
            public ActionResult RenderOperatingFranchises()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_operating_franchises.cshtml");
            }
            // Scholarships ticker share bar
            public ActionResult RenderTickerShare()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_tickershare.cshtml");
            }
            // Render the scholarships banner
            public ActionResult RenderScholarshipsBanner()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_banner_scholarships.cshtml");
            }
            // Render the website sidebar
            public ActionResult RenderSidebar()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_sidebar.cshtml");
            }

            // Render the cookies disclaimer html template where required
            public ActionResult RenderCookiesDisclaimer()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "_disclaimer.cshtml");
            }

            // Render the cookies disclaimer html template where required
            public ActionResult RenderInvestorSidebar()
            {
                return PartialView(PARTIAL_VIEW_FOLDER + "/sidebar/investors-sidebar.cshtml");
            }
        }
    }