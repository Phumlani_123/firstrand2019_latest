﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;


namespace Firstrand.Controllers
{
    public class SocietyController : SurfaceController
    {
        // Define the route for the partials
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Society/";

        // Render Investor presentations
        public ActionResult RenderSocialInvesting(){
            return PartialView(PARTIAL_VIEW_FOLDER + "_page_social_investing.cshtml");
        }
        // Render Monitoring and evaluations
        public ActionResult RenderMonitoringEvaluations(){
            return PartialView(PARTIAL_VIEW_FOLDER + "_loop_monitoring_and_evaluation.cshtml");
        }
        // Render Firstrand Foundations
        public ActionResult RenderFirstrandFoundations(){
            return PartialView(PARTIAL_VIEW_FOLDER + "_firstrand_foundations.cshtml");
        }
        // Render Firstrand Empowerment fund
        public ActionResult RenderEmpowermentFund(){
            return PartialView(PARTIAL_VIEW_FOLDER + "_empowerment_fund.cshtml");
        }

        // Render the _scholarship_applications partial
        public ActionResult RenderMonitoringAndEvaluation(){
            return PartialView(PARTIAL_VIEW_FOLDER + "_loop_monitoring_and_evaluation.cshtml");
        }
        // Render the scholarships index page
        public ActionResult RenderScholarshipsIndex(){
            return PartialView(PARTIAL_VIEW_FOLDER + "_page_scholarships_index.cshtml");
        }
        // Render the firstrand Volunteers gallery
        public ActionResult RenderFirstrandVolunteers()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_page_firstrand_volunteers.cshtml");
        }
        // Render the firstrand Volunteers single galleries
        public ActionResult RenderSingleGallery()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_loop_galleries.cshtml");
        }


        // Render the firstrand Volunteers single galleries
        public ActionResult RenderPresentations()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "/investors/_loop_presentations.cshtml");
        }
    }
} 